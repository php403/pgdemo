##require


##require-dev
- [barryvdh/laravel-ide-helper](https://gist.github.com/barryvdh/5227822). composer require barryvdh/laravel-ide-helper --dev
- [barryvdh/laravel-debugbar](https://packagist.org/packages/barryvdh/laravel-debugbar).composer require barryvdh/laravel-debugbar --dev
