<?php
/**
 * Created by PhpStorm.
 * User: jade
 * Date: 2017/9/29
 * Time: 9:34
 */
namespace Src\Controllers\Web\Company;

use Src\Model\EnterpriseMember;
use Illuminate\Http\Request;
use Src\Model\Repositories\EnterpriseMemberRepository;

class IndexController extends CompanyController
{
    private $_model;

    public function __construct() {
        parent::__construct();
        $this->_model = new EnterpriseMemberRepository();
    }

    /**
     * 企业后台登录
     * @param Request $request
     * @return array|bool
     */
    public function anyLogin(Request $request) {
        if ($request->isMethod('post')) {
            $rules = [
                'user_name' => 'required',
                'password' => 'required|min:6|max:30',
            ];
            $messages = [
                'user_name.required' => '用户名名不能为空',
                'password.required' => '密码不能为空',
                'password.min' => ' 密码不能少于6个字符',
                'password.max' => '密码不能超过30个字符',
                'contacts.required' => '联系人不能为空',
            ];

            if (($error = $this->validate($request, $rules, $messages)) !== true) return $error;

            $user_name  = $request->input('user_name');
            $password  = $request->input('password');
            $enterprise = EnterpriseMember::where('user_name', $user_name)->first();

            if ($enterprise == false) {
                return $this->errorJson('该用户不存在');
            }

            if ($enterprise->password != md5(md5(trim($password)).$enterprise->salt)) {
                return $this->errorJson('密码错误');
            }
            //纪录登录日志
            $data = array(
                'enterprise_id'=>$enterprise->id,
                'source_ip'=>$request->ip(),
                'status_type'=>1,
                'add_time'=>time(),
            );

            if (EnterpriseLoginLog::create($data)) {
                session(['enterprise_msg' => $enterprise->toArray() + ['loginTime' => time()]]);
                return $this->successJson($enterprise,200);
            }else{
                return $this->errorJson('登录失败');
            }
        }else{
            //查询出入住企业的logo
            $logo_img = $this->_model->enterComany();
            dd($logo_img);
//            return view('web.company.login',['logo_img'=>$logo_img]);
        }
    }
}