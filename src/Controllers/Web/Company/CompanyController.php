<?php
/**
 * Created by PhpStorm.
 * User: xiaofeibao
 * Date: 2017/9/29
 * Time: 9:36
 */

namespace Src\Controllers\Web\Company;


use Illuminate\Http\Request;
use Src\Controllers\Web\WebController;
use Illuminate\Support\Facades\Validator;

class CompanyController extends WebController
{
    public function __construct() {

    }

    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = []) {
        $validator = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if ($validator->fails()) {
            if ($request->ajax()) {
                return $this->errorJson($validator->errors()->first(), 412);
            } else {
                return $validator->errors()->first();
            }
        }
        return true;
    }
}