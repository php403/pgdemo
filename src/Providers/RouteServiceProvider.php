<?php

namespace Src\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Src\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group(
            ['namespace' => $this->namespace .'\Web', 'middleware' => 'web'],
            function () {
                Route::group(['namespace' => 'Pc', 'domain' => env_domain('www')], src_path('routes/web/pc.php'));
                Route::group(['namespace' => 'Mobile', 'domain' => env_domain('m')], src_path('routes/web/mobile.php'));
                Route::group(['namespace' => 'Manager', 'domain' => env_domain('manager')], src_path('routes/web/manager.php'));
                Route::group(['namespace' => 'Company', 'domain' => env_domain('company')], src_path('routes/web/company.php'));
            }
        );
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group(
            ['middleware' => 'api', 'namespace' => $this->namespace. '\Api', 'domain' => env_domain('api')],
            function(){
                Route::prefix('v4')->namespace('V4')->group(src_path('routes/api/v4.php'));
            }
        );
    }
}
