<?php

namespace Src\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->_repositoryBoot();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->_envRegister();
    }

    private function _envRegister()
    {
        if ($this->app->environment() != 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }

    private function _repositoryBoot()
    {
        $map = [
            'Tousu' => 'TousuRepository',
            'EnterpriseMember' => 'EnterpriseMemberRepository'
        ];

        foreach ($map as $Model=>$repository) {
            $Repository = "Src\Model\Repositories\{$repository}";

            $this->app->bind($Repository, function () use ($Repository, $Model){
                return new $Repository(new $Model);
            });
        }
    }

}
