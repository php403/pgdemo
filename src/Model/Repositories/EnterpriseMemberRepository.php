<?php
/**
 * Created by PhpStorm.
 * User: xiaofeibao
 * Date: 2017/9/29
 * Time: 10:19
 */

namespace Src\Model\Repositories;

use Src\Model\EnterpriseMember;

class EnterpriseMemberRepository extends BaseRepository
{
    public function __construct(EnterpriseMember $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * 入驻企业
     * @return mixed
     */
    public function enterComany() {
        return $this->model->where('record_status',4)->get();
    }
}