<?php

namespace Src\Model;

use Illuminate\Database\Eloquent\Model;

class EnterpriseMember extends Model {

    protected $table = 'enterprise_member';
    protected $guarded = ['id'];
    public $timestamps = false;

}
