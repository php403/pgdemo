<?php
/**
 * Created by PhpStorm.
 * User: gary
 * Date: 2017/9/26
 * Time: 09:52
 */

if (!function_exists('src_path')) {
    /**
     * 获取 src 目录
     * @param string $path
     * @return string
     */
    function src_path($path = '') {
        return base_path('src' . ($path ? DIRECTORY_SEPARATOR.$path : ''));
    }
}

if (!function_exists('env_domain')) {
    /**
     * 获取环境相关二级域名
     * @param $prix
     * @return string
     */
    function env_domain($prix) {
        $env = app()->environment();
        if ($env == 'production') return $prix . '.' . (env('APP_DOMAIN') ? : 'xfb315.com') ;

        return $prix . ".{$env}." . (env('APP_DOMAIN') ? : 'xfb315.com');
    }
}
